const http = require("http");
const path = require("path");
const { v4: uuidv4 } = require("uuid");
const { htmlCode, jsonData } = require(path.join(__dirname, "./data.js"));

const server = http.createServer((req, res) => {
  const url = req.url;
  let check = url.split('/')[1];
  switch (check) {
    case "html":
      res.writeHead(200, { "Content-Type": "text/html" });
      res.write(htmlCode);
      res.end();
      break;
    case "json":
      res.writeHead(200, { "Content-Type": "application/json" });
      res.write(JSON.stringify(jsonData));
      res.end();
      break;
    case "uuid":
      res.writeHead(200, { "Content-Type": "application/json" });
      const uuid = { uuid: uuidv4() };
      res.write(JSON.stringify(uuid));
      res.end();
      break;
    case "status":
      const status_code = url.split("/")[2];
      res.statusCode = parseInt(status_code);
      res.end(`Your status code is ${status_code}`);
      break;
    case "delay":
      const delay_in_seconds = parseInt(url.split("/")[2]);
      setTimeout(() => {
        res.statusCode = 200;
        res.end();
      }, delay_in_seconds * 1000);
      break;
    default:
      res.writeHead(404, { "Content-Type": "text/plain" });
      res.write("Page not found!");
      res.end();
  }
});
server.listen(3000, () => {
  console.log("Server listening on port 3000");
});
